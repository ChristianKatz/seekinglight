﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Suspicious : MonoBehaviour
{

    [SerializeField]
    private GameObject[] sword;

    [SerializeField]
    private GameObject pressFText;

    private bool decide;
    public bool Decide
    {
        get
        {
            return decide;
        }
        set
        {
            decide = value;
        }
    }

    private float arrestedCooldown = 3;

    private bool mageArrested = false;
    public bool MageArrested
    {
        get
        {
            return mageArrested;
        }
        set
        {
            mageArrested = value;
        }
    }

    [SerializeField]
    private TextMeshProUGUI arrestedText;
    public TextMeshProUGUI ArrestedText
    {
        get
        {
            return arrestedText;
        }
        set
        {
            arrestedText = value;
        }
    }

    private SwordExecution[] swordExecution;

    void Start()
    {
        sword[0].SetActive(false);
        arrestedText.gameObject.SetActive(false);
        swordExecution = FindObjectsOfType<SwordExecution>();
        pressFText.SetActive(false);

    }

    void Update()
    {
        MageArrestedText();
        ArrestedText.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("PlayerMove"))
        {
            sword[0].SetActive(true);
            pressFText.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PlayerMove"))
        {
            sword[0].SetActive(false);
            sword[1].SetActive(false);
            pressFText.SetActive(false);

            for(int i = 0; i < swordExecution.Length; i++)
            swordExecution[i].AskForArrestText.gameObject.GetComponent<TextMeshProUGUI>().enabled = false;
        }
    }

    void MageArrestedText()
    {
        if (mageArrested == true)
        {
            arrestedCooldown -= Time.deltaTime;

            if (arrestedCooldown <= 0)
            {
                arrestedText.gameObject.SetActive(false);
                arrestedCooldown = 3;
                mageArrested = false;
            }
        }
    }
}

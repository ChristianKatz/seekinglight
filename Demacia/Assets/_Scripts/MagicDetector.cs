﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// script is attached on every civilian
public class MagicDetector : MonoBehaviour
{
    [SerializeField]
    private GameObject blueImage;

    [SerializeField]
    private Transform playerPosition;

    private float distanceToMage;

    [SerializeField]
    private AudioSource[] citySounds;

    [SerializeField]
    private AudioSource magicSound;

    void Start()
    {
        blueImage.gameObject.SetActive(false);
        magicSound.Play();
    }

    void Update()
    {          
        distanceToMage = Vector3.Distance(transform.position, playerPosition.position);
        distantMage();

    }

     private void distantMage()
     {      
        if (distanceToMage < 20)
        {
            blueImage.gameObject.SetActive(true);          
        }

        if (distanceToMage > 20)
        {
            blueImage.gameObject.SetActive(false);           
        }

        if (distanceToMage < 40)
        {
            for (int i = 0; i < citySounds.Length; i++)
            {
                citySounds[i].volume = Mathf.Lerp(1, 0.1f, Time.time);
                
            }        
        }

        if (distanceToMage < 40)
        {
            magicSound.volume = Mathf.Lerp(0, 0.5f, Time.time);
        }

        if (distanceToMage > 40)
        {
            for (int i = 0; i < citySounds.Length; i++)
            {
                citySounds[i].volume = Mathf.Lerp(0.1f, 1, Time.time);
            }       
        }

        if (distanceToMage > 40)
        {
            magicSound.volume = Mathf.Lerp(0.5f, 0, Time.time);
        }
               
     }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AskForMageHint : MonoBehaviour
{
    private bool pressfButton = true;

    private bool onTrigger = false;

    [SerializeField]
    private TextMeshProUGUI infoText;

    [SerializeField]
    private TextMeshProUGUI pressFText;

    void Start()
    {
        pressFText.gameObject.SetActive(false);
        infoText.gameObject.SetActive(false);
    }

    void Update()
    {
        AskPerson();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("PlayerMove"))
        {
            pressFText.gameObject.SetActive(true);
            onTrigger = true;
        }


    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PlayerMove"))
        {
            onTrigger = false;
            pressFText.gameObject.SetActive(false);
            infoText.gameObject.SetActive(false);

        }
    }

    private void AskPerson()
    {
        if(onTrigger == true)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                if (pressfButton == true)
                {
                    pressFText.gameObject.SetActive(false);
                    infoText.gameObject.SetActive(true);
                    pressfButton = false;
                }
                else
                {
                    pressFText.gameObject.SetActive(true);
                    infoText.gameObject.SetActive(false);
                    pressfButton = true;
                }
            }
        }
    }
}

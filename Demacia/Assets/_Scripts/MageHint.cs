﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageHint : MonoBehaviour
{
    [SerializeField]
    private GameObject letterText;

    private bool fButton = true;

    private bool playerOnTrigger = false;

    [SerializeField]
    private GameObject pressFText;

    void Start()
    {
        letterText.SetActive(false);
        pressFText.SetActive(false);
    }

    void Update()
    {
        ReadHint();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerMove"))
        {
            pressFText.SetActive(true);
            playerOnTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PlayerMove"))
        {
            playerOnTrigger = false;
            letterText.SetActive(false);
            fButton = true;
            pressFText.SetActive(false);
        }
    }

    private void ReadHint()
    {
        if(playerOnTrigger == true)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                if (fButton == true)
                {
                    letterText.SetActive(true);
                    fButton = false;
                    pressFText.SetActive(false);
                }
                else
                {
                    letterText.SetActive(false);
                    fButton = true;
                    pressFText.SetActive(true);
                }
            }

        }

    }
}

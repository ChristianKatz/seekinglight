﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceNameFade : MonoBehaviour
{
    private bool fadeIn = false;

    private bool fadeOut = false;

    [SerializeField]
    private RectTransform outPosition;

    [SerializeField]
    private RectTransform inPosition;

    [SerializeField]
    private RectTransform text;

    private float speed = 200;

    void Update()
    {
        if(fadeIn == true)
        {
            text.position = Vector3.MoveTowards(text.position, inPosition.position, speed * Time.deltaTime);
        }

        else if (fadeOut == true)
        {
            text.position = Vector3.MoveTowards(text.position, outPosition.position, speed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("PlayerMove"))
        {
            fadeOut = false;
            fadeIn = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PlayerMove"))
        {
            fadeIn = false;
            fadeOut = true;
        }
    }
}

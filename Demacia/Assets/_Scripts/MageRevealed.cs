﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

//script is attached on the sword
public class MageRevealed : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI youLost;

    [SerializeField]
    private GameObject menu;

    [SerializeField]
    private GameObject mage;

    private MageCounter mageCounter;

    private Suspicious suspicious;

    void Start()
    {
        mageCounter = FindObjectOfType<MageCounter>();
        youLost.gameObject.SetActive(false);
        suspicious = FindObjectOfType<Suspicious>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Wizard"))
        {
            mageCounter.MageCounterNumber++;           
            Destroy(mage, 3);
            suspicious.ArrestedText.gameObject.SetActive(true);
            suspicious.MageArrested = true;
        }

        else if (other.CompareTag("Innocent"))
        {
            youLost.gameObject.SetActive(true);
            Time.timeScale = 0;
            menu.SetActive(true);
            suspicious.ArrestedText.gameObject.SetActive(false);

        }
    }
}

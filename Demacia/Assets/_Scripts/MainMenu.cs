﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private string sceneNameForPlay;

    [SerializeField]
    private string sceneNameForMainMenu;

    [SerializeField]
    private GameObject introduction;

    void Start()
    {
        introduction.SetActive(false);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void Play()
    {
        SceneManager.LoadScene(sceneNameForPlay);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Back()
    {
        SceneManager.LoadScene(sceneNameForMainMenu);
    }

    public void HowToPlay()
    {
        introduction.SetActive(true);
    }
}
